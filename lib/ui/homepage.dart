import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:division/division.dart';
import 'userCard.dart';

class HomePage extends StatelessWidget {
  var iconscolor = const Color(0xff56e39c);
  var darkergray = const Color(0xff292732);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: new Stack(
        children: <Widget>[
          new Image.asset('images/VegaWalletBuildingBlocks.png',
              fit: BoxFit.contain),

          Padding(
            padding: const EdgeInsets.only(top:100.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: WalletCard(),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new MaterialButton(
                    height: 80.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: iconscolor,
                    onPressed: () {},
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Icon(
                          Icons.monetization_on,
                          color: Colors.white,
                          size: 50.0,
                        ),
                        new Text(
                          'Buy Cryptocurrency',
                          style: new TextStyle(color: Colors.white,fontSize: 25.0),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
          //_buildUserRow()
        ],
      ),
    );
  }
}
