import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';

math.Random random = new math.Random();

List<double> _generateRandomData(int count) {
  List<double> result = <double>[];
  for (int i = 0; i < count; i++) {
    result.add(random.nextDouble() * 100);
  }
  return result;
}

class Chart extends StatelessWidget {
  var data = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];
  var primary = const Color(0xff313342);
  var iconscolor = const Color(0xff56e39c);
  var darkergray = const Color(0xff292732);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipRRect(
      borderRadius: BorderRadius.circular(17.0),
      child: new Container(
        height: 200.0,
        width: double.maxFinite,
        child: new Sparkline(
          data: data,
              lineColor:iconscolor ,
              fillMode: FillMode.below,
              fillColor: primary,

        ),
      ),
    );
  }






}