import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:vgw/ui/homepage.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
 var primary = const Color(0xff313342);
 var iconscolor = const Color(0xff56e39c);
 var darkergray = const Color(0xff292732);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var currentIndex;
    var _selectedIndex = 0;
   var  _pageController = new PageController() ;
    List<Widget> Pages = [HomePage() ] ;
    return new Scaffold(
      backgroundColor: primary,
        body: new Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Pages[_selectedIndex],
        ),
        bottomNavigationBar: new BottomNavyBar(
          backgroundColor: primary,
          selectedIndex: _selectedIndex,
          showElevation: true, // use this to remove appBar's elevation
          onItemSelected: (index) => setState(() {
                _selectedIndex = 0;

                _pageController.animateToPage(index,
                    duration: Duration(milliseconds: 300), curve: Curves.ease);
              }),
          items: [
            BottomNavyBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
              activeColor: iconscolor,
            ),
            BottomNavyBarItem(
                icon: Icon(Icons.account_balance),
                title: Text('Users'),
                activeColor: iconscolor),
            BottomNavyBarItem(
                icon: Icon(Icons.account_balance_wallet),
                title: Text('Messages'),
                activeColor: iconscolor),
            BottomNavyBarItem(
                icon: Icon(Icons.filter_frames),
                title: Text('Messages'),
                activeColor: iconscolor),
            BottomNavyBarItem(
                icon: Icon(Icons.settings),
                title: Text('Settings'),
                activeColor: iconscolor),
          ],
        ));
  }
}
