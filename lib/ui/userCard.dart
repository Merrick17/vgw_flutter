import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'charts.dart';
class WalletCard extends StatelessWidget {
  var iconscolor = const Color(0xff56e39c);
  var darkergray = const Color(0xff292732);
  Widget _buildUserRow() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

            new FlatButton.icon(onPressed: (){}, icon: new Icon(Icons.remove_red_eye,color: Colors.white,), label:new Text("")),
            Align( alignment: Alignment.topRight,
                child: new FlatButton.icon(onPressed: (){}, icon: new Icon(Icons.refresh,color: Colors.white,), label:new Text(""))),

          ],
        ),
        new Center(
          child: new Text('Total Portfolio Value :',style: new TextStyle(
            fontSize: 19.0,
            fontFamily: "Roboto"
          ),),
        ),
        new Center(
          child: new Text('109.23 USD ',style: new TextStyle(
              fontSize: 25.0,
              fontFamily: "Roboto"
              ,fontWeight: FontWeight.bold,
              color: iconscolor
          ),),
        ),
        new Chart()
      ],
    );
  }





  @override
  Widget build(BuildContext context) {
    return Parent(
      style: userCardStyle,
      child: _buildUserRow()

    );
  }

  //Styling

  final ParentStyle userCardStyle = ParentStyle()
    ..height(370)
    ..padding(horizontal: 5.0, vertical: 5)
    ..alignment.center()
    ..background.hex('#292732')
    ..borderRadius(all: 17.0);
    //..elevation(10, color: hex('fffffff'));

  final ParentStyle userImageStyle = ParentStyle()
    ..height(50)
    ..width(50)
    ..margin(right: 10.0)
    ..borderRadius(all: 30)
    ..background.hex('ffffff');

  final ParentStyle userStatsStyle = ParentStyle()..margin(vertical: 10.0);

  final TextStyle nameTextStyle = TextStyle(
      color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.w600);

  final TextStyle nameDescriptionTextStyle =
  TextStyle(color: Colors.white.withOpacity(0.6), fontSize: 12.0);
}
