import 'package:flutter/material.dart';

import 'ui/dashboard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

   return MaterialApp(
      theme: ThemeData.dark(),
    home: new Dashboard(),
   );
  }
}

